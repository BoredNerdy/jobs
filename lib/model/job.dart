
import 'dart:convert';

List<Job> jobFromJson(String str) => List<Job>.from(json.decode(str).map((x) => Job.fromJson(x)));

String jobToJson(List<Job> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Job {
  Job({
    required this.contents,
    required this.name,
    required this.type,
    required this.publicationDate,
    required this.shortName,
    required this.modelType,
    required this.id,
    required this.locations,
    required this.refs,
    required this.company,
  });

  String contents;
  String name;
  String type;
  DateTime publicationDate;
  String shortName;
  String modelType;
  int id;
  String locations;
  String refs;
  String company;

  factory Job.fromJson(Map<String, dynamic> json) => Job(
    contents: json["contents"],
    name: json["name"],
    type: json["type"],
    publicationDate: DateTime.parse(json["publication_date"]),
    shortName: json["short_name"],
    modelType: json["model_type"],
    id: json["id"],
    locations: json["locations"],
    refs: json["refs"],
    company: json["company"],
  );

  Map<String, dynamic> toJson() => {
    "contents": contents,
    "name": name,
    "type": type,
    "publication_date": publicationDate.toIso8601String(),
    "short_name": shortName,
    "model_type": modelType,
    "id": id,
    "locations": locations,
    "refs": refs,
    "company": company,
  };
}
